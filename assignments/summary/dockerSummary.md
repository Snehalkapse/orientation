## Docker overview

Docker is a container management service. The keywords of Docker are develop, ship and run anywhere. The whole idea of Docker is for developers to easily develop applications, ship them into containers which can then be deployed anywhere. By taking advantage of Docker's methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

The initial release of Docker was in March 2013 and since then, it has become the buzzword for modern world development, especially in the face of Agile-based projects.

<h4>Features of Docker</h4>

 * Docker has the ability to reduce the size of development by providing a smaller footprint of the operating system via      containers.
 * With containers, it becomes easier for teams across different units, such as development, QA and Operations to work seamlessly across applications.
  * You can deploy Docker containers anywhere, on any physical and virtual machines and even on the cloud.
  * Since Docker containers are pretty lightweight, they are very easily scalable.

<h4>Docker – Container Lifecycle</h4>
The following illustration explains the entire lifecycle of a Docker container.

![Docker Container Lifecycle](extras/docker_lifecycle.png)

  * Initially, the Docker container will be in the **created** state.
  * Then the Docker container goes into the running state when the Docker **run** command is used.
  * The Docker **kill** command is used to kill an existing Docker container.
  * The Docker pause command is used to **pause** an existing Docker container.
  * The Docker **stop** command is used to pause an existing Docker container.
  * The Docker **run** command is used to put a container back from a **stopped** state to a **running** state.

The following image shows the standard and traditional architecture of virtualization.

![Traditional Architecture of Virtualization](extras/architecture_of_vitualization.png)

 * The server is the physical server that is used to host multiple virtual machines.
 * The Host OS is the base machine such as Linux or Windows.
 * The Hypervisor is either VMWare or Windows Hyper V that is used to host virtual machines.
 * You would then install multiple operating systems as virtual machines on top of the existing hypervisor as Guest OS.
 * You would then host your applications on top of each Guest OS.

The following image shows the new generation of virtualization that is enabled via Dockers. Let’s have a look at the various layers.

![New Neneration of Virtualization](extras/new_gen_virtualization.png)

 * The server is the physical server that is used to host multiple virtual machines. So this layer remains the same.
 * The Host OS is the base machine such as Linux or Windows. So this layer remains the same.
 * Now comes the new generation which is the Docker engine. This is used to run the operating system which earlier used to be virtual machines as Docker containers.
 * All of the Apps now run as Docker containers.
 * The clear advantage in this architecture is that you don’t need to have extra hardware for Guest OS. Everything works as Docker containers.
## Images
An image is a read-only template with instructions for creating a Docker container. Often, an image is based on another image, with some additional customization. For example, you may build an image which is based on the ubuntu image, but installs the Apache web server and your application, as well as the configuration details needed to make your application run.

You might create your own images or you might only use those created by others and published in a registry. To build your own image, you create a Dockerfile with a simple syntax for defining the steps needed to create the image and run it. Each instruction in a Dockerfile creates a layer in the image. When you change the Dockerfile and rebuild the image, only those layers which have changed are rebuilt. This is part of what makes images so lightweight, small, and fast, when compared to other virtualization technologies.

## Containers
A container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI. You can connect a container to one or more networks, attach storage to it, or even create a new image based on its current state.

By default, a container is relatively well isolated from other containers and its host machine. You can control how isolated a container's network, storage, or other underlying subsystems are from other containers or from the host machine.

A container is defined by its image as well as any configuration options you provide to it when you create or start it. When a container is removed, any changes to its state that are not stored in persistent storage disappear.

## Docker Basic Commands
- To list all the running containers on the docker host: `docker ps`
- To start any stopped container: `docker start <container-id>` or `docker start <container-name>`
- To stop a running container: `docker stop <container-id>` or `docker stop <container-name>`
- To create a container from an image: `docker run <container-name>`
- To delete a container: `docker rm <container-name>`
***





