<h3>What is Git?</h3>
So, what is Git in a nutshell? This is an important section to absorb, because if you understand what Git is and the fundamentals of how it works, then using Git effectively will probably be much easier for you. As you learn Git, try to clear your mind of the things you may know about other VCSs, such as CVS, Subversion or Perforce-doing so will help you avoid subtle confusion when using the tool. Even though Git's user interface is fairly similar to these other VCSs, Git stores and thinks about information in a very different way, and understanding these differences will help you avoid becoming confused while using it.

The major difference between Git and any other VCS (Subversion and friends included) is the way Git thinks about its data. Conceptually, most other systems store information as a list of file-based changes. These other systems (CVS, Subversion, Perforce, Bazaar, and so on) think of the information they store as a set of files and the changes made to each file over time (this is commonly described as delta-based version control).

![](extras/git1.jpg)

<h3>What is version control?</h3>

Git is, first and foremost, a version control system (VCS). There are many version control systems out there: CVS, SVN, Mercurial, Fossil, and, of course, Git.

Git serves as the foundation for many services, like GitHub and GitLab, but you can use Git without using any other service. This means that you can use Git privately or publicly.

If you have ever collaborated on anything digital with anyone, then you know how it goes. It starts out simple: you have your version, and you send it to your partner. They make some changes, so now there are two versions, and send the suggestions back to you. You integrate their changes into your version, and now there is one version again.

Then it gets worse: while you change your version further, your partner makes more changes to their version. Now you have three versions; the merged copy that you both worked on, the version you changed, and the version your partner has changed.

As Jason van Gumster points out in his article, Even artists need version control, this syndrome tends to happen in individual settings as well. In both art and science, it's not uncommon to develop a trial version of something; a version of your project that might make it a lot better, or that might fail miserably. So you create file names like **project_justTesting.kdenlive** and **project_betterVersion.kdenlive**, and then **project_best_FINAL.kdenlive**, but with the inevitable allowance for **project_FINAL-alternateVersion.kdenlive**, and so on.

Whether it's a change to a for loop or an editing change, it happens to the best of us. That is where a good version control system makes life easier.

<h3>Git snapshots</h3>

Git takes snapshots of a project, and stores those snapshots as unique versions. If you go off in a direction with your project that you decide was the wrong direction, you can just roll back to the last good version and continue along an alternate path. If you're collaborating, then when someone sends you changes, you can merge those changes into your working branch, and then your collaborator can grab the merged version of the project and continue working from the new current version.



<h3>Git distributes</h3>

Working on a project on separate machines is complex, because you want to have the latest version of a project while you work, makes your own changes, and share your changes with your collaborators. The default method of doing this tends to be clunky online file sharing services, or old school email attachments, both of which are inefficient and error-prone.

Git is designed for distributed development. If you're involved with a project you can clone the project's Git repository, and then work on it as if it was the only copy in existence. Then, with a few simple commands, you can pull in any changes from other contributors, and you can also push your changes over to someone else. Now there is no confusion about who has what version of a project, or whose changes exist where. It is all locally developed, and pushed and pulled toward a common target (or not, depending on how the project chooses to develop).

<h3>Git interfaces</h3>

In its natural state, Git is an application that runs in the Linux terminal. However, as it is well-designed and open source, developers all over the world have designed other ways to access it.

It is free, available to anyone for $0, and comes in packages on Linux, BSD, Illumos, and other Unix-like operating systems. It looks like this:

$ git --version
git version 2.5.3
Probably the most well-known Git interfaces are web-based: sites like GitHub, the open source GitLab, Savannah, BitBucket, and SourceForge all offer online code hosting to maximise the public and social aspect of open source along with, in varying degrees, browser-based GUIs to minimise the learning curve of using Git. This is what the GitLab interface looks like:

![](extras/gitlab_interface.png)

Additionally, it is possible that a Git service or independent developer may even have a custom Git frontend that is not HTML-based, which is particularly handy if you don't live with a browser eternally open. The most transparent integration comes in the form of file manager support. The KDE file manager, **Dolphin**, can show the Git status of a directory, and even generate commits, pushes, and pulls.

![](extras/dolphin.jpg)

**Sparkleshare** uses Git as a foundation for its own Dropbox-style file sharing interface.
![](extras/sparkleshare.jpg)



<h2>Some Git Basic Commands</h2>


* **_git checkout_**  command is used to switch between branches in a repository.  
* **_git add_**  is a command used to add a file that is in the working directory to the staging area.  
* **_git commit_**  is a command used to add all files that are staged to the local repository.  
* **_git push_**  is a command used to add all committed files in the local repository to the remote repository. So in the remote repository, all files and changes will be visible to anyone with access to the remote repository.  
* **_git fetch_**  is a command used to get files from the remote repository to the local repository but not into the working directory.  
* **_git merge_**  is a command used to get the files from the local repository into the working directory.  
* **_git pull_**  is command used to get files from the remote repository directly into the working directory. It is equivalent to a _git fetch_ and a _git merge_.  
